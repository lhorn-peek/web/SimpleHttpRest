﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.SimpleRest.Config;
using Web.SimpleRest.Framework;

namespace Web.SimpleRest
{
    public class RestClientFactory : IRestClientFactory
    {
        public IRestApiClient NewClient()
        {
            return ApiConfiguration.ConfigureApi();
        }
    }
}
