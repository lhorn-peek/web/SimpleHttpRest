﻿using Web.SimpleRest.Framework;

namespace Web.SimpleRest
{
    public interface IRestClientFactory
    {
        IRestApiClient NewClient();
    }
}