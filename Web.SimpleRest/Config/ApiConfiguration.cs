﻿using Web.SimpleRest.Framework;

namespace Web.SimpleRest.Config
{
    internal class ApiConfiguration
    {
        public static IRestApiClient ConfigureApi()
        {
            return new RestApiClient();
        }
    }
}
