﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Web.SimpleRest.Framework
{
    public class RestApiClient : IRestApiClient
    {
        #region fields

        public Dictionary<string, string> RequestHeaders { get; set; }

        public List<string> AcceptHeaders { get; set; }

        public string Address { get; set; }

        public HttpClientHandler HttpClientHandler { get; set; }

        public List<Cookie> Cookies { get; set; }

        public RestApiClient()
        {
            AcceptHeaders = new List<string>();
            RequestHeaders = new Dictionary<string, string>();
            Cookies = new List<Cookie>();
        }

        #endregion


        public void UseHandler(HttpClientHandler clientHandler)
        {
            HttpClientHandler = clientHandler;
        }


        public RestResponse PostJson<T>(string apiResource, T postObject)
        {
            return AsyncHelper.RunSync(() => PostJsonAsync(apiResource, postObject));
        }

        public async Task<RestResponse> PostJsonAsync<T>(string apiResource, T postObject)
        {
            RestResponse restResponse;

            using (var client = SetupClient())
            {
                var response = await client.PostAsync(apiResource, FormatRequestBody(postObject));

                restResponse = await HandleResponse(response);
            }

            return restResponse;
        }



        public async Task<RestResponse> PostContentAsync(string apiResource, HttpContent content)
        {
            RestResponse restResponse;

            using (var client = SetupClient())
            {
                var response = await client.PostAsync(apiResource, content);

                restResponse = await HandleResponse(response);
            }

            return restResponse;
        }


        public RestResponse PostContent(string apiResource, HttpContent content)
        {
            return AsyncHelper.RunSync(() => PostContentAsync(apiResource, content));
        }


        public RestResponse PutJson<T>(string apiResource, T putObject)
        {
            return AsyncHelper.RunSync(() => PutJsonAsync(apiResource, putObject));
        }

        public async Task<RestResponse> PutJsonAsync<T>(string apiResource, T putObject)
        {
            RestResponse restResponse;

            using (var client = SetupClient())
            {
                var response = await client.PutAsync(apiResource, FormatRequestBody(putObject));

                restResponse = await HandleResponse(response);
            }

            return restResponse;
        }

        public RestResponse PutText(string apiResource, string text)
        {
            return AsyncHelper.RunSync(() => PutTextAsync(apiResource, text));
        }

        public async Task<RestResponse> PutTextAsync(string apiResource, string text)
        {
            RestResponse restResponse;

            using (var client = SetupClient())
            {
                var response = await client.PutAsync(apiResource, FormatRequestBody(text));

                restResponse = await HandleResponse(response);
            }

            return restResponse;
        }

        public RestResponse GetJson(string apiResource)
        {
            return AsyncHelper.RunSync(() => GetJsonAsync(apiResource));
        }

        public async Task<RestResponse> GetJsonAsync(string apiResource)
        {
            RestResponse restResponse;

            using (var client = SetupClient())
            {
                var response = await client.GetAsync(apiResource);

                restResponse = await HandleResponse(response);
            }

            return restResponse;
        }

        public void AddHeader(KeyValuePair<string, string> header)
        {
            //check for nulls or empty
            RequestHeaders.Add(header.Key, header.Value);
        }

        public void AddAcceptHeader(string value)
        {
            //check for nulls or empty
            AcceptHeaders.Add(value);
        }

        private HttpClient SetupClient()
        {
            if(HttpClientHandler != null & Cookies.Count > 0)
            {
                HttpClientHandler.CookieContainer = new CookieContainer();
                foreach (var ck in Cookies)
                    HttpClientHandler.CookieContainer.Add(new Uri(Address), ck);
            }

            var client = HttpClientHandler == null ? new HttpClient() : new HttpClient(HttpClientHandler);

            client.BaseAddress = new Uri(Address);

            if (AcceptHeaders?.Count > 0)
            {
                client.DefaultRequestHeaders.Accept.Clear();
                foreach (var header in AcceptHeaders)
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(header));
            }

            if (RequestHeaders?.Count > 0)
            {
                foreach (var header in RequestHeaders)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            return client;
        }

        private static async Task<RestResponse> HandleResponse(HttpResponseMessage responseHttp)
        {
            var moduleResponse = new RestResponse(responseHttp);

            if (responseHttp.StatusCode == HttpStatusCode.OK)
            {
                moduleResponse.Body = await responseHttp.Content.ReadAsStringAsync();
            }

            return moduleResponse;
        }

        private static StringContent FormatRequestBody<T>(T obj)
        {
            return new StringContent(
                JsonConvert.SerializeObject(obj),
                Encoding.UTF8,
                "application/json");
        }

        private static StringContent FormatRequestBody(string rawText)
        {
            return new StringContent(
                rawText,
                Encoding.UTF8, "application/octet-stream");
        }
    }
}
