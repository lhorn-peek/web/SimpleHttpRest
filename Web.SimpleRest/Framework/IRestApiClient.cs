﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Web.SimpleRest.Framework
{
    public interface IRestApiClient
    {
        List<string> AcceptHeaders { get; set; }
        Dictionary<string, string> RequestHeaders { get; set; }
        string Address { get; set; }
        HttpClientHandler HttpClientHandler { get; set; }
        List<Cookie> Cookies { get; set; }

        void AddAcceptHeader(string value);
        void AddHeader(KeyValuePair<string, string> header);
        void UseHandler(HttpClientHandler clientHandler);
        RestResponse PostJson<T>(string apiResource, T postObject);
        Task<RestResponse> PostJsonAsync<T>(string apiResource, T postObject);

        Task<RestResponse> PostContentAsync(string apiResource, HttpContent content);

        RestResponse PostContent(string apiResource, HttpContent content);

        RestResponse PutJson<T>(string apiResource, T putObject);
        Task<RestResponse> PutJsonAsync<T>(string apiResource, T putObject);

        RestResponse PutText(string apiResource, string text);
        Task<RestResponse> PutTextAsync(string apiResource, string text);

        RestResponse GetJson(string apiResource);
        Task<RestResponse> GetJsonAsync(string apiResource);

    }
}
