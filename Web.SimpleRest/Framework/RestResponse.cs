﻿using System.Net.Http;
using Newtonsoft.Json;

namespace Web.SimpleRest.Framework
{
    public class RestResponse
    {
        private object _bodyAsType;

        public RestResponse(HttpResponseMessage response)
        {
            HttpResponse = response;
        }

        public HttpResponseMessage HttpResponse { get; set; }

        public string Body { get; set; }

        public T BodyAsType<T>()
        {
            if (string.IsNullOrEmpty(Body))
                return default(T);

            if (_bodyAsType == null)
                _bodyAsType = JsonConvert.DeserializeObject<T>(Body);

            return (T)_bodyAsType;
        }

    }
}
